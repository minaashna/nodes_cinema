//
//  UIImageView.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/13/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import UIKit

protocol ImageFetcher {
    func imageWithURL(_ urlString: String?)
}

extension UIImageView: ImageFetcher {
    func imageWithURL(_ urlString: String?) {
        guard let str = urlString else {
            self.backgroundColor = .lightGray
            self.image = nil
            return
        }

        DataManager.shared.getImage(str) { [weak self] (requestedURL, imageData, error) in
            if error != nil || imageData == nil {
                self?.backgroundColor = .lightGray
                self?.image = nil
                return
            }
            
            if requestedURL == str, let image = UIImage(data: imageData!) {
                self?.image = image
            }
        }
    }
}
