//
//  UIView.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/14/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import UIKit

extension UIView {
    func showLoading() {
        let activityIndicator = UIActivityIndicatorView()
        createLoadingView(with: activityIndicator)
        activityIndicator.startAnimating()
    }
    
    func hideLoading() {
        if let loadingView = self.viewWithTag(100) {
            loadingView.removeFromSuperview()
        }
    }
    
    private func createLoadingView(with activityIndicator: UIActivityIndicatorView) {
        let loadingView = UIView(frame: self.bounds)
        loadingView.tag = 100
        loadingView.layer.cornerRadius = self.layer.cornerRadius
        loadingView.backgroundColor = self.backgroundColor == nil ?
            .white : self.backgroundColor
        self.addSubview(loadingView)
        customizeActivityIndicator(with: activityIndicator,
                                   on: loadingView)
    }
    
    private func customizeActivityIndicator(with activityIndicator: UIActivityIndicatorView,
                                            on loadingView: UIView) {
        activityIndicator.hidesWhenStopped = true
        loadingView.backgroundColor = .white
        activityIndicator.color = .gray
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        loadingView.addSubview(activityIndicator)
        center(activityIndicator, on: loadingView)
    }
    
    private func center(_ activityIndicator: UIActivityIndicatorView, on loadingView: UIView) {
        let xCenterConstraint = NSLayoutConstraint(item: loadingView,
                                                   attribute: .centerX,
                                                   relatedBy: .equal,
                                                   toItem: activityIndicator,
                                                   attribute: .centerX,
                                                   multiplier: 1,
                                                   constant: 0)
        self.addConstraint(xCenterConstraint)
        
        let yCenterConstraint = NSLayoutConstraint(item: loadingView,
                                                   attribute: .centerY,
                                                   relatedBy: .equal,
                                                   toItem: activityIndicator,
                                                   attribute: .centerY,
                                                   multiplier: 1,
                                                   constant: 0)
        self.addConstraint(yCenterConstraint)
    }
    
}
