//
//  UIView+Error.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/14/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import UIKit

extension UIView {
    func showError(with msg: String) {
        let errorLabel = UILabel()
        errorLabel.textColor = .black
        errorLabel.text = msg
        errorLabel.tag = 200
        createLoadingView(with: errorLabel)
    }
    
    func hideError() {
        if let errorLabel = self.viewWithTag(200) {
            errorLabel.removeFromSuperview()
        }
    }
    
    private func createLoadingView(with label: UILabel) {
        let backgroundView = UIView(frame: self.bounds)
        backgroundView.tag = 100
        backgroundView.layer.cornerRadius = self.layer.cornerRadius
        backgroundView.backgroundColor = self.backgroundColor == nil ?
            .white : self.backgroundColor
        self.addSubview(backgroundView)
        label.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.addSubview(label)
        center(label, on: backgroundView)
    }
    
    private func center(_ label: UILabel, on backgroundView: UIView) {
        let xCenterConstraint = NSLayoutConstraint(item: backgroundView,
                                                   attribute: .centerX,
                                                   relatedBy: .equal,
                                                   toItem: label,
                                                   attribute: .centerX,
                                                   multiplier: 1,
                                                   constant: 0)
        self.addConstraint(xCenterConstraint)
        
        let yCenterConstraint = NSLayoutConstraint(item: backgroundView,
                                                   attribute: .centerY,
                                                   relatedBy: .equal,
                                                   toItem: label,
                                                   attribute: .centerY,
                                                   multiplier: 1,
                                                   constant: 0)
        self.addConstraint(yCenterConstraint)
    }
}
