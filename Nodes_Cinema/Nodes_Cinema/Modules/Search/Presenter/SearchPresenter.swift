//
//  SearchPresenter.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/13/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

class SearchPresenter {
    weak var userInterface: SearchControllerModuleInterface?
    
    init(with userInterface: SearchControllerModuleInterface) {
        self.userInterface = userInterface
    }
}

extension SearchPresenter: SearchPresenterModuleInterface {
    func search(query: String) {
        userInterface?.showLoading()
        DataManager.shared.getMovies(from: query) { [weak self] (searchResult, error) in
            if let apiError = error {
                self?.userInterface?.updateUI(with: apiError)
                return
            }
            
            guard let searchResult = searchResult else {
                return
            }
            self?.userInterface?.updateUI(with: searchResult)
        }
    }
    
    func moveToDetails(with indexPath: IndexPath) {
        userInterface?.moveToDetails(with: indexPath)
    }
}

