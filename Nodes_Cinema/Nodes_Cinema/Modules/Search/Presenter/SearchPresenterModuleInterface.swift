//
//  SearchPresenterModuleInterface.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/13/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

protocol SearchPresenterModuleInterface {
    
    /// Requesting a movie by name
    ///
    /// - Parameter query: the name of the movie which has to be searched.
    func search(query: String)

    /// Navigating the user to details view to see the movie information
    ///
    /// - Parameter indexPath: the selected movie indexPath
    func moveToDetails(with indexPath: IndexPath)
}
