//
//  SearchControllerModuleInterface.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/13/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

protocol SearchControllerModuleInterface: class {
    func updateUI(with movies: SearchResult)
    func updateUI(with error: Error)
    func moveToDetails(with indexPath: IndexPath)
    func showLoading()
}
