//
//  SearchController.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/12/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import UIKit

class SearchController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: SearchPresenterModuleInterface?
    var searchTableViewDelegate: SearchTableViewDelegate?
    var searchTableViewDataSource: SearchTableViewDataSource?
    var searchBarDelegate: UISearchBarDelegate?
    
    var movies: [SearchMovie] = [] {
        didSet {
            searchTableViewDelegate?.dataSource = movies
            searchTableViewDataSource?.dataSource = movies
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
}

extension SearchController {
    @IBAction func favoriteButtonDidTap(_ sender: UIBarButtonItem) {
        if let favoriteController = UIStoryboard(name: "Favorite", bundle: nil).instantiateViewController(withIdentifier: "FavoriteController") as? FavoriteController {
            favoriteController.presenter = FavoritePresenter(with: favoriteController)
            navigationController?.pushViewController(favoriteController, animated: true)
        }
    }
}

extension SearchController {
    private func setupView() {
        if let presenter = presenter {
            searchTableViewDelegate = SearchTableViewDelegate(with: presenter,
                                                              and: tableView)
            tableView.delegate = searchTableViewDelegate
            searchTableViewDataSource = SearchTableViewDataSource(with: presenter,
                                                                  and: tableView)
            tableView.dataSource = searchTableViewDataSource
            searchBarDelegate = SearchBarDelegate(with: presenter)
            searchBar.delegate = searchBarDelegate
        }
    }
}

extension SearchController: SearchControllerModuleInterface {
    func updateUI(with error: Error) {
        self.tableView.hideLoading()
        self.tableView.showError(with: error.localizedDescription)
    }
    
    func updateUI(with movies: SearchResult) {
        self.tableView.hideLoading()
        guard let result = movies.results else {
            return
        }
        self.movies = result
    }
    
    func moveToDetails(with indexPath: IndexPath) {
        if let detailsController = UIStoryboard(name: "Details", bundle: nil).instantiateViewController(withIdentifier: "DetailsController") as? DetailsController {
            detailsController.presenter = DetailsPresenter(with: detailsController,
                                                           and: movies[indexPath.row].id)
            navigationController?.pushViewController(detailsController, animated: true)
        }
    }
    
    func showLoading() {
        self.tableView.showLoading()
    }
}
