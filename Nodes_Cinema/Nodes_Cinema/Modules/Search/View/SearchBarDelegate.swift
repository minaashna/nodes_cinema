//
//  SearchBarDelegate.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/14/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import UIKit

class SearchBarDelegate: NSObject {
    private var presenter: SearchPresenterModuleInterface!
    
    init(with presenter: SearchPresenterModuleInterface) {
        self.presenter = presenter
    }
}

extension SearchBarDelegate: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let query = searchBar.text else {
            return
        }
        presenter.search(query: query)
    }
}
