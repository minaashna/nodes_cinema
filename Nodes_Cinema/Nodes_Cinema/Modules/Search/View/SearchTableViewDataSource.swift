//
//  SearchTableViewDataSource.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/14/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import UIKit

class SearchTableViewDataSource: NSObject {
    private var presenter: SearchPresenterModuleInterface?
    private weak var tableView: UITableView!
    
    var dataSource: [SearchMovie]? {
        didSet {
            tableView.reloadData()
        }
    }
    
    required init(with presenter: SearchPresenterModuleInterface, and tableView: UITableView) {
        self.presenter = presenter
        self.tableView = tableView
        
        let movieCell = UINib(nibName: "MovieCell", bundle: nil)
        self.tableView.register(movieCell, forCellReuseIdentifier: "MovieCell")
    }
}

extension SearchTableViewDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as? MovieCell else {
            fatalError()
        }
        if let dataSource = dataSource {
            cell.setContent(with: dataSource[indexPath.row])
        }
        return cell
    }
}
