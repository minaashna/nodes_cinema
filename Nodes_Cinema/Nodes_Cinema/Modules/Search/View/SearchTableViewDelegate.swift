//
//  SearchTableViewDelegate.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/14/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import UIKit

class SearchTableViewDelegate: NSObject {
    private weak var tableView: UITableView!
    private var presenter: SearchPresenterModuleInterface?
    
    var dataSource: [SearchMovie]?

    required init(with presenter: SearchPresenterModuleInterface, and tableView: UITableView) {
        self.presenter = presenter
        self.tableView = tableView
    }
}

extension SearchTableViewDelegate: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.bounds.height / 4
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.moveToDetails(with: indexPath)
    }
}
