//
//  SearchCell.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/12/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {

    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var overview: UILabel!
    
    override func prepareForReuse() {
        movieImage.image = nil
        title.text = ""
        releaseDate.text = ""
        overview.text = ""
    }
}

extension MovieCell {
    func setContent(with movie: SearchMovie) {
        self.movieImage.imageWithURL(movie.imageURL)
        self.title.text = movie.title
        self.releaseDate.text = movie.releaseDate
        self.overview.text = movie.overview
    }
    
    func setContent(with movie: DetailsMovie) {
        self.movieImage.imageWithURL(movie.imageURL)
        self.title.text = movie.title
        self.releaseDate.text = movie.releaseDate
        self.overview.text = movie.overview
    }
}
