//
//  Movie.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/12/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

class Movie: Codable {
    var voteCount: Int?
    var id: Int!
    var video: Bool?
    var voteAverage: Float?
    var title: String?
    var popularity: Float?
    var posterPath: String?
    var originalLanguage: String?
    var originalTitle: String?
    var backdropPath: String?
    var adult: Bool?
    var overview: String?
    var releaseDate: String?

    private enum CodingKeys: String, CodingKey {
        case voteCount = "vote_count"
        case id, video
        case voteAverage = "vote_average"
        case title, popularity
        case posterPath = "poster_path"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case backdropPath = "backdrop_path"
        case adult
        case overview
        case releaseDate = "release_date"
    }
    
    func encode(to encoder: Encoder) throws { var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(title, forKey: .title)
        try container.encode(releaseDate, forKey: .releaseDate)
        try container.encode(overview, forKey: .overview)
        try container.encode(posterPath, forKey: .posterPath)
    }
}

extension Movie {
    var imageURL: String? {
        return "https://image.tmdb.org/t/p/w500/\(posterPath ?? "")"
    }
}
