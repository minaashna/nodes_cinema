//
//  SearchResult.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/13/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

class SearchResult: Codable {
    var page: Int?
    var totalResults: Int?
    var totalPages: Int?
    var results: [SearchMovie]?
    
    enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results
    }
}

extension SearchResult {
    static func parseData(_ serverResponse: Data?) -> SearchResult? {
        guard let data = serverResponse else {
            return nil
        }
        do {
            let result = try JSONDecoder().decode(SearchResult.self, from: data)
            return result
        } catch {
            return nil
        }
    }
}
