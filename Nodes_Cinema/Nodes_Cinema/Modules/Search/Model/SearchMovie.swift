//
//  SearchMovie.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/13/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

class SearchMovie: Movie {
    var genreIDs: [Int]?
    
    private enum CodingKeys: String, CodingKey {
        case genreIDs = "genre_ids"
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)

        let container = try decoder.container(keyedBy: CodingKeys.self)
        genreIDs = try container.decode([Int].self, forKey: .genreIDs)
    }
}
