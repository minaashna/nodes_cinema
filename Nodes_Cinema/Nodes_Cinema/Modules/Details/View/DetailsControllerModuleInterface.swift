//
//  DetailsControllerModuleInterface.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/13/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

protocol DetailsControllerModuleInterface: class {
    func updateUI(with movie: DetailsMovie)
    func updateUI(with error: Error)
    func makeFavorite()
    func makeUnFavorite()
}
