//
//  DetailsController.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/13/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import UIKit

class DetailsController: UIViewController {
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var genre: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var budget: UILabel!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var overview: UILabel!
    
    var favoriteButton: UIBarButtonItem!
    var presenter: DetailsPresenterModuleInterface?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.showLoading()
        presenter?.getMovieDetails()
    }
}

extension DetailsController {
    @IBAction func favoritButtonTapped(_ sender: UIBarButtonItem) {
        presenter?.favoriteButtonDidTap()
    }
}

extension DetailsController {
    func setupView() {
        self.navigationController?.navigationBar.tintColor = .black
        makeUnFavorite()
    }
}

extension DetailsController: DetailsControllerModuleInterface {
    func makeFavorite() {
        favoriteButton = UIBarButtonItem(image: #imageLiteral(resourceName: "favoriteFill"), style: .plain, target: self, action: #selector(favoritButtonTapped(_:)))
        favoriteButton.tintColor = .red
        navigationItem.rightBarButtonItem = favoriteButton
    }
    
    func makeUnFavorite() {
        favoriteButton = UIBarButtonItem(image: #imageLiteral(resourceName: "favorite"), style: .plain, target: self, action: #selector(favoritButtonTapped(_:)))
        favoriteButton.tintColor = .black
        navigationItem.rightBarButtonItem = favoriteButton
    }
    
    func updateUI(with movie: DetailsMovie) {
        if let presenter = presenter, presenter.isFavorite() {
            makeFavorite()
        }
        self.view.hideLoading()
        movieImage.imageWithURL(movie.imageURL)
        if let genres = movie.genres {
            let genreString = genres.lazy.filter { genre -> Bool in
                return genre.id != nil
                }.map { (genre) -> String in
                    return genre.name ?? "none"
                }.joined(separator: ", ")
            genre.text = "Genres: \(genreString)"
        }
        status.text = "Status: \(movie.status ?? "")"
        releaseDate.text = "Release Date: \(movie.releaseDate ?? "")"
        budget.text = "Budget: \(movie.budget?.description ?? "")"
        movieTitle.text = movie.title
        overview.text = movie.overview
    }
    
    func updateUI(with error: Error) {
        self.view.hideLoading()
        self.view.showError(with: error.localizedDescription)
    }
}
