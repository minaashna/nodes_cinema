//
//  DetailsPresenter.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/13/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

class DetailsPresenter {
    weak var userInterface: DetailsControllerModuleInterface?
    var movie: DetailsMovie?
    private var id: Int!
    
    init(with userInterface: DetailsControllerModuleInterface, and id: Int) {
        self.userInterface = userInterface
        self.id = id
    }
}

extension DetailsPresenter: DetailsPresenterModuleInterface {
    func favoriteButtonDidTap() {
        guard let movie = movie else {
            return
        }
        do {
            guard !DataManager.shared.isFavorit(movie.id) else {
                try DataManager.shared.deleteFromFavorites(movie.id)
                userInterface?.makeUnFavorite()
                return
            }
            
            try DataManager.shared.addToFavorites(movie)
            userInterface?.makeFavorite()
        } catch {
            userInterface?.updateUI(with: error)
        }
    }
    
    func isFavorite() -> Bool {
        guard let movie = movie else {
            return false
        }
        return DataManager.shared.isFavorit(movie.id)
    }
    
    func getMovieDetails() {
        DataManager.shared.getMovieDetails(with: id) { [weak self] (result, error) in
            if let apiError = error {
                self?.userInterface?.updateUI(with: apiError)
                return
            }
            guard let result = result else {
                return
            }
            self?.movie = result
            self?.userInterface?.updateUI(with: result)
        }
    }
    
    
}
