//
//  DetailsPresenterModuleInterface.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/13/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

protocol DetailsPresenterModuleInterface {
    
    /// Requesting the movie details
    func getMovieDetails()
    
    /// is the current movie in favorite list or not
    ///
    /// - Returns: true to show it is existed and false to show not exist
    func isFavorite() -> Bool
    
    /// Favorite process after the user touch the favorite button
    func favoriteButtonDidTap()
}
