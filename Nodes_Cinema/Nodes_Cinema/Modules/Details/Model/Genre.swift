//
//  Genre.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/13/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

class Genre: Codable {
    var id: Int?
    var name: String?
    
    init(id: Int?, name: String?) {
        self.id = id
        self.name = name
    }
    
    private enum CodingKeys: String, CodingKey {
        case id, name
    }
}

extension Genre {
    static func modelFromDictionary(_ dic: [String: Any]) -> Genre {
        return Genre(id: dic["id"] as? Int,
                     name: dic["name"] as? String)
    }
    
    static func parseData(_ serverResponse: [[String: Any]]?) -> [Genre]? {
        guard let genres = serverResponse, !genres.isEmpty else {
            return nil
        }
        
        let result = genres.filter { (genreDic) -> Bool in
            if genreDic["id"] != nil {
                return true
            }
            return false
            
            }.map { (genreDic) -> Genre in
                return Genre.modelFromDictionary(genreDic)
        }
        
        return result
    }
}
