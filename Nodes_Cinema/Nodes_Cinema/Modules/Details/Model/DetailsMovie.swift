//
//  DetailsMovie.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/13/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

class DetailsMovie: Movie {
    //    var belongsToCollection: ?
    var budget: Double?
    var genres: [Genre]?
    var homepage: String?
    var imdbID: String?
    var productionCompanies: [Company]?
    var status: String?
    var tagline: String?
    
    enum CodingKeys: String, CodingKey {
        //        case belongsToCollection = "belongs_to_collection"
        case budget, genres, homepage, id
        case imdbID = "imdb_id"
        case productionCompanies = "production_companies"
        case status, tagline
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        budget = try container.decode(Double.self, forKey: .budget)
        genres = try container.decode([Genre].self, forKey: .genres)
        homepage = try container.decode(String.self, forKey: .homepage)
        imdbID = try container.decode(String.self, forKey: .imdbID)
        productionCompanies = try container.decode([Company].self, forKey: .productionCompanies)
        status = try container.decode(String.self, forKey: .status)
        tagline = try container.decode(String.self, forKey: .tagline)

    }

}

extension DetailsMovie {
    static func parseData(_ serverResponse: Data?) -> DetailsMovie? {
        guard let movie = serverResponse else {
            return nil
        }
        do {
            let result = try JSONDecoder().decode(DetailsMovie.self, from: movie)
            return result
        } catch {
            return nil
        }
    }
}
