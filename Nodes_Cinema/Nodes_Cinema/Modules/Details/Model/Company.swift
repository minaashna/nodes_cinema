//
//  Company.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/13/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

class Company: Codable {
    var id: Int?
    var logoPath: String?
    var name: String?
    var originalCountry: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case logoPath = "logo_path"
        case name
        case originalCountry = "origin_country"
    }
    
    init(id: Int?, logoPath: String?, name: String?, originalCountry: String?) {
        self.id = id
        self.logoPath = logoPath
        self.name = name
        self.originalCountry = originalCountry
    }
}
