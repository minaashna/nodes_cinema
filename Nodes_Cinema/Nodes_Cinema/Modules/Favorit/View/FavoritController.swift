//
//  FavoritController.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/14/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import UIKit

class FavoriteController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var presenter: FavoritePresenterModuleInterface?
    var favoriteTableViewDelegate: FavoriteTableViewDelegate?
    var favoriteTableViewDataSource: FavoriteTableViewDataSource?
    
    var favoriteMovies: [DetailsMovie] = [] {
        didSet {
            favoriteTableViewDelegate?.dataSource = favoriteMovies
            favoriteTableViewDataSource?.dataSource = favoriteMovies
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let favoriteMovies = presenter?.getFavorites() {
            self.favoriteMovies = favoriteMovies
        }
    }
}

extension FavoriteController {
    func setupView() {
        self.navigationController?.navigationBar.tintColor = .black
        if let presenter = presenter {
            favoriteTableViewDelegate = FavoriteTableViewDelegate(with: presenter, and: tableView)
            tableView.delegate = favoriteTableViewDelegate
            favoriteTableViewDataSource = FavoriteTableViewDataSource(with: presenter, and: tableView)
            tableView.dataSource = favoriteTableViewDataSource
        }
    }
}

extension FavoriteController: FavoriteControllerModuleInterface {
    func moveToDetails(with indexPath: IndexPath) {
        if let detailsController = UIStoryboard(name: "Details", bundle: nil).instantiateViewController(withIdentifier: "DetailsController") as? DetailsController {
            detailsController.presenter = DetailsPresenter(with: detailsController,
                                                           and: favoriteMovies[indexPath.row].id)
            navigationController?.pushViewController(detailsController, animated: true)
        }
    }
}
















