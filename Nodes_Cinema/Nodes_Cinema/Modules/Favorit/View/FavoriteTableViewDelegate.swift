//
//  FavoriteTableViewDelegate.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/14/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import UIKit

class FavoriteTableViewDelegate: NSObject {
    private weak var tableView: UITableView!
    private var presenter: FavoritePresenterModuleInterface?
    
    var dataSource: [DetailsMovie]?
    
    required init(with presenter: FavoritePresenterModuleInterface, and tableView: UITableView) {
        self.presenter = presenter
        self.tableView = tableView
    }
}

extension FavoriteTableViewDelegate: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.bounds.height / 4
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.moveToDetails(with: indexPath)
    }
}
