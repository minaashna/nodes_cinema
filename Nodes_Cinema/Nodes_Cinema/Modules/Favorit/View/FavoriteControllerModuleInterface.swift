//
//  FavoritControllerModuleInterface.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/14/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

protocol FavoriteControllerModuleInterface: class {
    func moveToDetails(with indexPath: IndexPath)
}
