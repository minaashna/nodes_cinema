//
//  FavoritPresenterModuleInterface.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/14/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

protocol FavoritePresenterModuleInterface: class {
    
    /// Getting the user favorite list from storage
    ///
    /// - Returns: list of favorite movies
    func getFavorites() -> [DetailsMovie]?
    
    /// Navigating the user to details view to see the movie information
    ///
    /// - Parameter indexPath: the selected movie indexPath
    func moveToDetails(with indexPath: IndexPath)
}
