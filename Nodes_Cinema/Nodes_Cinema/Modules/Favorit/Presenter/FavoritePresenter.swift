//
//  FavoritPresenter.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/14/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

class FavoritePresenter {
    weak var userInterface: FavoriteControllerModuleInterface?
    
    init(with userInterface: FavoriteControllerModuleInterface) {
        self.userInterface = userInterface
    }
}

extension FavoritePresenter: FavoritePresenterModuleInterface {
    func getFavorites() -> [DetailsMovie]? {
        return DataManager.shared.getFavorites()
    }

    func moveToDetails(with indexPath: IndexPath) {
        userInterface?.moveToDetails(with: indexPath)
    }
}
