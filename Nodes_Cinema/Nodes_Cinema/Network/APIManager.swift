//
//  APIManager.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/12/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation
import Alamofire

enum APIManagerError: Error {
    case invalidImageURL
}

extension APIManagerError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .invalidImageURL:
            return "Image URL is not valid."
        }
    }
}

class APIManager {
    
    /// Getting data from specified URL
    ///
    /// - Parameters:
    ///   - url: target url
    ///   - completion: response in Data or Error format
    func get(from url: URL,
             withCompletionBlock completion: @escaping (Data?, Error?) -> Void) {
        Alamofire.request(url, method: .get,
                          parameters: nil).validate().responseJSON(queue: DispatchQueue.global(qos: .userInitiated),
                                                                   options: .allowFragments) { (responseData) in
                                                                    
                                                                    switch responseData.result {
                                                                    case .success:
                                                                        print("Validation Successful")
                                                                        completion(responseData.data, nil)
                                                                        
                                                                    case .failure(let error):
                                                                        print(error)
                                                                        completion(nil, responseData.error)
                                                                    }
        }
    }
    
    /// Downloading image from remote server
    ///
    /// - Parameters:
    ///   - urlString: image url
    ///   - completion: response type in Data or Error format
    func downloadItemImage(withUrl urlString: String?, completionBlock completion: @escaping (Data?, Error?) -> Void) {
        guard let url = urlString, !url.isEmpty else {
            completion(nil, APIManagerError.invalidImageURL)
            return
        }
        
        Alamofire.request(url, method: .get, parameters: nil).validate().responseData(queue: DispatchQueue.global(qos: .background)) { (responseData) in
            
            switch responseData.result {
            case .success:
                print("Validation Successful")
                completion(responseData.value, nil)
                
            case .failure(let error):
                print(error)
                completion(nil, responseData.error)
            }
        }
    }
}
