//
//  DataManager.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/12/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

typealias ImageDownloadHandler = (_ requestedUrl: String?, _ image: Data?, _ error: Error?) -> Void

enum DataManagerError: Error {
    case noNetwork
    case invalidImage
    case invalidURL
    case emptyResponse
    case invalidResponse
    case storageFail
}

extension DataManagerError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .noNetwork:
            return "No network connection."
        case .invalidImage:
            return "Image result is not valid."
        case .invalidURL:
            return "URL is not valid."
        case .emptyResponse:
            return "Server response is empty."
        case .invalidResponse:
            return "Server response is not valid."
        case .storageFail:
            return "Storage process failed."
        }
    }
}

class DataManager {
    static let shared = DataManager()
    let apiManager = APIManager()
    let connectionManager = ConnectionManager()
    let searchURL = "https://api.themoviedb.org/3/search/movie?api_key=4cb1eeab94f45affe2536f2c684a5c9e&query="
    let detailsURL = "https://api.themoviedb.org/3/movie/%@?api_key=4cb1eeab94f45affe2536f2c684a5c9e"
    let kFavoriteListStorage = "kFavoriteListStorage"
    
    private var _cacheFavorites: [Int: DetailsMovie] = [:]
    var cacheFavorites: [Int: DetailsMovie] {
        get {
            guard _cacheFavorites.isEmpty else {
                return self._cacheFavorites
            }
            guard let favoritesData = UserDefaults.standard.object(forKey: kFavoriteListStorage) as? Data else {
                return [:]
            }
            
            guard let _cacheFavorites = try? JSONDecoder().decode([Int: DetailsMovie].self, from: favoritesData) else {
                return [:]
            }
            self._cacheFavorites = _cacheFavorites
            return _cacheFavorites
        }
        
        set {
            self._cacheFavorites = newValue
        }
    }
}

extension DataManager {
    public func getImage(_ urlString: String?, _ completion: @escaping (String?, Data?, Error?) -> Void ) {
        
        guard connectionManager.isNetworkReachable() else {
            completion(urlString, nil, DataManagerError.noNetwork)
            return
        }
        
        apiManager.downloadItemImage(withUrl: urlString) { (result, error) in
            let completionOnMainThread: ImageDownloadHandler = { (rUrl, data, err) in
                if let imageData = data {
                    DispatchQueue.main.async {
                        completion(rUrl, imageData, err)
                    }
                }
            }
            guard error == nil else {
                
                completionOnMainThread(urlString, nil, error)
                return
            }
            
            if let imgData = result {
                completionOnMainThread(urlString, imgData, nil)
            } else {
                completionOnMainThread(urlString, nil, DataManagerError.invalidImage)
            }
        }
    }
}

extension DataManager: DataManagerNetworkProtocol {
    func getMovies(from queryString: String, withCompletion completion: @escaping (SearchResult?, Error?) -> Void) {
        guard connectionManager.isNetworkReachable() else {
            completion(nil, DataManagerError.noNetwork)
            return
        }
        
        let encodeString = queryString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        guard let url = URL(string: "\(searchURL)\(encodeString ?? "")") else {
            completion(nil, DataManagerError.invalidURL)
            return
        }
        apiManager.get(from: url) { (response, error) in
            let completionOnMainThread: (SearchResult?, Error?) -> Void = { (response, error) in
                OperationQueue.main.addOperation {
                    completion(response, error)
                }
            }
            guard error == nil else {
                completionOnMainThread(nil, error)
                return
            }
            
            guard let serverResponse = response else {
                completionOnMainThread(nil, DataManagerError.emptyResponse)
                return
            }
            
            if let result = SearchResult.parseData(serverResponse) {
                completionOnMainThread(result, nil)
                return
            }
            
            completionOnMainThread(nil, DataManagerError.invalidResponse)
        }
        
    }

    func getMovieDetails(with id: Int, withCompletion completion: @escaping (DetailsMovie?, Error?) -> Void) {
        guard connectionManager.isNetworkReachable() else {
            completion(nil, DataManagerError.noNetwork)
            return
        }
        
        guard let url = URL(string: String(format: detailsURL, "\(id)")) else {
            completion(nil, DataManagerError.invalidURL)
            return
        }

        apiManager.get(from: url) { (response, error) in
            let completionOnMainThread: (DetailsMovie?, Error?) -> Void = { (response, error) in
                OperationQueue.main.addOperation {
                    completion(response, error)
                }
            }
            
            guard error == nil else {
                completionOnMainThread(nil, error)
                return
            }
            
            guard let serverResponse = response else {
                completionOnMainThread(nil,  DataManagerError.emptyResponse)
                return
            }
            
            if let result = DetailsMovie.parseData(serverResponse) {
                completionOnMainThread(result, nil)
                return
            }
            
            completionOnMainThread(nil, DataManagerError.invalidResponse)
        }
    }
}

extension DataManager: DataManagerStorageProtocol {
    func isFavorit(_ id: Int) -> Bool {
        guard !cacheFavorites.isEmpty else {
            return false
        }
        return cacheFavorites[id] != nil
    }
    
    func getFavorites() -> [DetailsMovie]? {
        guard !cacheFavorites.isEmpty else {
            return nil
        }
        return cacheFavorites.map { (key, value) in
            return value
        }
    }
    
    func deleteFromFavorites(_ id: Int) throws {
        var tempCacheFavorites = cacheFavorites
        tempCacheFavorites.removeValue(forKey: id)
        cacheFavorites = tempCacheFavorites
        guard !tempCacheFavorites.isEmpty else {
            UserDefaults.standard.removeObject(forKey: kFavoriteListStorage)
            return
        }
        do {
            let encodedObject = try JSONEncoder().encode(cacheFavorites)
            UserDefaults.standard.set(encodedObject, forKey: kFavoriteListStorage)
        } catch {
            throw DataManagerError.storageFail
        }
    }
    
    func addToFavorites(_ movie: DetailsMovie) throws {
        cacheFavorites[movie.id] = movie
        do {
            let encodedObject = try JSONEncoder().encode(cacheFavorites)
            UserDefaults.standard.set(encodedObject, forKey: kFavoriteListStorage)
        } catch {
            throw DataManagerError.storageFail
        }
    }

}
