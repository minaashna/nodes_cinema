//
//  DataManagerStorageProtocol.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/14/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

protocol DataManagerStorageProtocol {
    
    /// Adding the movie to favorites list in cache and storage
    ///
    /// - Parameter movie: the favorited movie
    /// - Throws: throw the error
    func addToFavorites(_ movie: DetailsMovie) throws
    
    /// Getting favorites list from cache or storage
    ///
    /// - Returns: user favorites list
    func getFavorites() -> [DetailsMovie]?
    
    /// Deleting a favotited movie from both cache and storage
    ///
    /// - Parameter id: the favorited movie id
    /// - Throws: throw the error
    func deleteFromFavorites(_ id: Int) throws
    
    /// Check whether the movie is favorited or not
    ///
    /// - Parameter id: the movie id
    /// - Returns: true means it is favorited and false means it is not favorited
    func isFavorit(_ id: Int) -> Bool
}
