//
//  DataManagerNetworkProtocol.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/14/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation

protocol DataManagerNetworkProtocol {
    /// Getting movies from specified URL
    ///
    /// - Parameters:
    ///   - url: target url
    ///   - completion: response in SearchResult or Error format
    func getMovies(from queryString: String, withCompletion completion: @escaping (SearchResult?, Error?) -> Void)
    
    /// Getting movie details with id
    ///
    /// - Parameters:
    ///   - id: the movie id
    ///   - completion: response in DetailsMovie or Error format
    func getMovieDetails(with id: Int, withCompletion completion: @escaping (DetailsMovie?, Error?) -> Void)
}
