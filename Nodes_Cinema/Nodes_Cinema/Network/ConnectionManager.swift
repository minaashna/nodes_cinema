//
//  ConnectionManager.swift
//  Nodes_Cinema
//
//  Created by Mina Ashena on 4/13/18.
//  Copyright © 2018 Mina Ashna. All rights reserved.
//

import Foundation
import Alamofire

class ConnectionManager {
    fileprivate var reachabilityManager: NetworkReachabilityManager?
    init() {
        reachabilityManager = NetworkReachabilityManager(host: "www.google.com")
    }
}

extension ConnectionManager {
    func isNetworkReachable() -> Bool {
        return reachabilityManager?.isReachable ?? false
    }
}
